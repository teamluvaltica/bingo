<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Cartones;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CartonesTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(Cartones::class);

        $component->assertStatus(200);
    }
}
