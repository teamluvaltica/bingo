<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Bingo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class BingoTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(Bingo::class);

        $component->assertStatus(200);
    }
}
