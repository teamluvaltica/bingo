<div>
    <div class="row pb-5">
        <div class="col-md-12">
            @foreach ($resultados as $items)
                <div class="row">
                    @foreach ($items as $item)
                        <div class="col-1-10 resulado align-middle"
                            style="min-width: 10%; min-height: 60px; text-align: center;">
                            <span class="badge {{ $select }} animated heartBeat rounded-circle py-2 px-2"
                                style="font-size: 24px; margin-top: 0px;">
                                <strong>{{ str_pad($item, 2, '0', STR_PAD_LEFT) }}</strong>
                            </span>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>

    <script>
        window.onload = function() {
            $(".badge").on("click", function(evt) {
                $this.removeClass("badge-success");
                $this.addClass("active").blur();
            });
        };
    </script>
</div>
