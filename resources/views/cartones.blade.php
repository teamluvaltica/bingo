@extends("layout")
@section("content")
    <div class="container pt-3">
        <h4 class="text-danger mb-0"><strong>Carton Bingo Kata</strong></h4>
        <hr>
        @livewire("cartones")
    </div>
@endsection
