@extends("layout")
@section("content")
    <div class="container pt-3">
        <h4 class="text-danger mb-0"><strong>Bienvenidos Bingo Kata</strong></h4>
        <hr>
        @livewire("bingo")
    </div>
@endsection