<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Cartones extends Component
{
    public $resultados;
    public $select = "badge-secondary";

    public function mount()
    {
        $this->inicializar();
    }

    private function inicializar()
    {
        $this->resultados = [];
        $rand = range(1, 75);
        shuffle($rand);

        $count=1;
        foreach ($rand as $val) {
            $this->resultados[] = $val;
            if ($count == 25) {
                break;
            }
            $count++;
        }
        $this->resultados = array_chunk($this->resultados, 5);
    }

    public function marcar()
    {
        $this->select = "badge-success";
    }

    public function render()
    {
        return view('livewire.cartones');
    }
}
